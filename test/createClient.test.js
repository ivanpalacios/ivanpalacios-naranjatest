const createClient = require('../src/client/domain/createClient');
const { createService } = require('../src/client/service/createClientService');
const { emitClient } = require('../src/client/service/emitClient');
const { getOne } = require('../src/client/service/getClientService');

jest.mock('../src/client/service/createClientService');
jest.mock('../src/client/service/emitClient');
jest.mock('../src/client/service/getClientService');

describe('When a client is created with ok params', () => {
    it('should succefully create a client', () => {
        const meta = {
            source: 'CLIENT_COMMAND',
            tracedDuration: {
              order: undefined,
              source: undefined,
              time: undefined,
              accumDuration: undefined
            }
        };
        createService.mockReturnValue(true);
        emitClient.mockReturnValue(true);
        getOne.mockReturnValue(false);

        const params = {
            "dni": "11111111",
            "name": "test",
            "lastName": "test",
            "birthday": "02-26-1991"
        };
        const expctedResult = {
            "dni":"11111111",
            "name":"test",
            "lastName":"test",
            "birthday":"02-26-1991",
            "age":30
        };
        return createClient(params, meta).then(data => {
            expect(data.body.payload).toStrictEqual(expctedResult);
        });
    });
});
const { createPaymentService } = require('../src/payment/service/createPaymentService');
const { emitNewPayment } = require('../src/payment/service/emitNewPayment');
const { getClientByDNI } = require('../src/payment/service/getClientService');
const createPayment = require('../src/payment/domain/createPayment');

jest.mock('../src/payment/service/createPaymentService');
jest.mock('../src/payment/service/emitNewPayment');
jest.mock('../src/payment/service/getClientService');

describe('When a payment is created with ok params', () => {
    it('should succefully create a payment', () => {
        const meta = {
            source: 'CLIENT_COMMAND',
            tracedDuration: {
              order: undefined,
              source: undefined,
              time: undefined,
              accumDuration: undefined
            }
        };
        const client = {
            Item: {
                "dni": "44444444",
                "active": true,
                "card": {
                    "type": "CLASSIC"
                }
            }
        };
        createPaymentService.mockReturnValue(true);
        emitNewPayment.mockReturnValue(true);
        getClientByDNI.mockReturnValue(client);

        const params = {
            "dni": "11111111",
            "products": [
                {
                    "name": "pollo con papas",
                    "price": 100
                }
            ]
        };

        return createPayment(params, meta).then(data => {
            expect(data.body.message).toStrictEqual('OK');
        });
    });
});
const { DownstreamEvent } = require('ebased/schema/downstreamEvent');

class EmitClientSchema extends DownstreamEvent {
    constructor(payload, meta) {
        super({
            type: 'New Client',
            specversion: 'v1.0.0',
            payload,
            meta,
            schema: {
                strict: false,
                dni: { type: String, required: true },
                name: { type: String, required: false },
                lastName: { type: String, required: false },
                birthday: { type: Date, required: false },
                age: { type: Number, required: true },
                cvvCard: { type: String, required: false},
                numberCard: { type: String, required: false }
            }
        });
    };
};

module.exports = { EmitClientSchema };
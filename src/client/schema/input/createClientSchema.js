const { InputValidation } = require('ebased/schema/inputValidation');

class CreateClientSchema extends InputValidation {
    constructor(payload, meta) {
        super({
            source: meta.status,
            payload,
            specversion: "v1.0.0",
            schema: {
                strict: false,
                dni: { type: String, required: true },
                name: { type: String, required: true },
                lastName: { type: String, required: true },
                birthday: { type: Date, required: true }
            }
        })
    }
}

module.exports = { CreateClientSchema };
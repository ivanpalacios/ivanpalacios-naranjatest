const { InputValidation } = require('ebased/schema/inputValidation');

class UpdateClientSchema extends InputValidation {
    constructor(payload, meta) {
        super({
            source: meta.status,
            payload,
            specversion: "v1.0.0",
            schema: {
                strict: false,
                dni: { type: String, required: true },
                birthday: { type: Date, required: false },
                name: { type: String, required: false },
                lastName: { type: String, required: false }
            }
        })
    }
}

module.exports = { UpdateClientSchema };
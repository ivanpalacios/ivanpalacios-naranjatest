const { InputValidation } = require('ebased/schema/inputValidation');

class DeleteClientSchema extends InputValidation {
    constructor(payload, meta) {
        super({
            source: meta.status,
            payload,
            specversion: "v1.0.0",
            schema: {
                strict: false,
                dni: { type: String, required: true },
            }
        })
    }
}

module.exports = { DeleteClientSchema };
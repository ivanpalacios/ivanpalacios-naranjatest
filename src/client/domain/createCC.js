const { updateService } = require('../service/updateClientService');

const { chooseCard, generateCC, generateCVV } = require('../helper/generateCC')

module.exports = async (eventPayload, _eventMeta, _rawEvent) => {
    const body = JSON.parse(eventPayload.Message);
    let item;
    item = {
        dni: body.dni,
        card : {
            type: chooseCard(body.age),
            cvv: body.cvvCard || generateCVV(),
            number: body.numberCard || generateCC()
        },
        active: true,
        updatedAt: Date.now()
    };
    
    await updateService(item);
};
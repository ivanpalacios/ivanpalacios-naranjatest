const { getGift } = require("../helper/getSeasonGift");
const { updateService } = require('../service/updateClientService');

module.exports = async (eventPayload) => {
    const body = JSON.parse(eventPayload.Message);
    const gift = {
        dni: body.dni,
        gift: getGift(body.birthday),
        updatedAt: Date.now()
    };

    await updateService(gift);
};
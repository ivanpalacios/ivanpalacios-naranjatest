const { CreateClientSchema } = require('../schema/input/createClientSchema');

const { calculateAge } = require('../helper/calculateAge');

const { createService } = require('../service/createClientService');
const { getOne } = require('../service/getClientService');
const { ErrorHandled } = require('ebased/util/error');

const { emitClient } = require('../service/emitClient');
const { EmitClientSchema } = require('../schema/event/emitClientSchema');


module.exports = async (commandPayload, commandMeta) => {

    new CreateClientSchema(commandPayload, commandMeta);

    const { Item } = await getOne(commandPayload.dni);
    if (Item) throw new ErrorHandled('This client already exist!', { code: 'CLIENT_EXIST', status: 400, layer: 'DOMAIN' });

    const age = calculateAge(commandPayload.birthday);
    if (age < 18 || age > 65) throw new ErrorHandled('You can not have a credit card', { code: 'CREATE_CLIENT_BAD_REQUEST', layer: 'DOMAIN', status: 400 });

    await createService({ dni: commandPayload.dni, active: false, createdAt: Date.now(),...commandPayload });
    commandPayload.age = age;
    await emitClient(new EmitClientSchema(commandPayload, commandMeta));

    return {
        body: {
            message: 'New client created',
            payload: commandPayload
        },
        status: 200
    }
};
const { ErrorHandled } = require("ebased/util/error");
const { DeleteClientSchema } = require("../schema/input/deleteClientSchema");
const { getOne } = require("../service/getClientService");
const { updateService } = require('../service/updateClientService');

module.exports = async (commandPayload, commandMeta) => {
    new DeleteClientSchema(commandPayload, commandMeta);

    const { Item } = await getOne(commandPayload.dni);
    if (!Item) throw new ErrorHandled('Client not found', { status: 400, code: "CLIENT_NOT_FOUND", layer: 'DOMAIN' })

    await updateService({ dni: commandPayload.dni, active: false });

    return {
        status: 200,
        body: {
            message: 'Client deleted',
        }
    };
};
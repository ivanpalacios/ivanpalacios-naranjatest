const { calculateAge } = require('../helper/calculateAge');

const { EmitClientSchema } = require('../schema/event/emitClientSchema');
const { UpdateClientSchema } = require('../schema/input/updateClientSchema');

const { emitClient } = require('../service/emitClient');

const { getOne } = require('../service/getClientService');
const { updateService } = require('../service/updateClientService');
const { ErrorHandled } = require('ebased/util/error');

module.exports = async (commandPayload, commandMeta) => {
    new UpdateClientSchema(commandPayload, commandMeta);

    const { Item } = await getOne(commandPayload.dni);
    if (!Item) throw new ErrorHandled('The client dni does not exist', { status: 400, code: 'CLIENT_NOT_FOUND', layer: 'DOMAIN' });
    await updateService({ dni: commandPayload.dni, updatedAt: Date.now(),...commandPayload });
    
    if (commandPayload.birthday) {
        commandPayload.cvvCard = Item.card ? Item.card.cvv : null;
        commandPayload.numberCard = Item.card ? Item.card.number : null;
        commandPayload.age = calculateAge(commandPayload.birthday);
        await emitClient(new EmitClientSchema(commandPayload, commandMeta));
    };
    return {
        status: 200,
        body: {
            message: 'Client updated',
        }
    };
};
const { getAll } = require('../service/getClientService');

module.exports = async (_commandPayload, _commandMeta) => {

    const { Items } = await getAll();
    return {
        status: 200,
        body: Items
    };
}
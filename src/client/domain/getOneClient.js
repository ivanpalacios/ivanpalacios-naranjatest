const { ErrorHandled } = require('ebased/util/error');
const { GetOneClientSchema } = require('../schema/input/getOneClientSchema');
const { getOne } = require('../service/getClientService');

module.exports = async (commandPayload, commandMeta) => {
    new GetOneClientSchema(commandPayload, commandMeta);
    console.log(commandPayload);
    const { Item } = await getOne(commandPayload.dni);
    if (!Item) throw new ErrorHandled('Client not found', { status: 400, code: 'NOT_FOUND', layer: "DOMAIN" })
    return {
        status: 200,
        body: Item
    };
};
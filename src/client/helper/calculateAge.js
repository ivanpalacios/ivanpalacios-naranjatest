const calculateAge = (date) => {
    const currentDate = new Date();
    const birthDay = new Date(date);
    const alreadyBirthday = currentDate.getMonth() - birthDay.getMonth()
    let age = currentDate.getFullYear() - birthDay.getFullYear();
    if (alreadyBirthday < 0 || (alreadyBirthday === 0 && currentDate.getTime() < birthDay.getTime())) {
        age--;
    }
    return age;
}

module.exports = { calculateAge };
const chooseCard = (age) => {
    if (age > 45) {
        return 'GOLD'
    } else {
        return 'CLASSIC'
    };
};

const generateCVV = () => {
    return (Math.floor(Math.random() * (999 - 101)) + 100).toString(10);
};

const generateCC = () => {
    return '4444555566667777'
};

module.exports = { chooseCard, generateCC, generateCVV }
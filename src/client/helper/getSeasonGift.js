const getSeason = (date) => {
    const birthday = new Date(date);
    const day = birthday.getDate();
    const month = birthday.getMonth();
    if (month < 2 || (month === 2 && day < 20)) {
        return 'Summer';
    } else if (month < 5 || (month === 5 && day < 21)) {
        return 'Fall'
    } else if (month < 8 || (month === 8 && day < 22)) {
        return 'Winter';
    } else if (month < 11 || (month === 11 && day < 21)) {
        return 'Spring';
    } else {
        return 'Summer';
    };
};

const getGift = (date) => {
    const season = getSeason(date);
    switch(season) {
        case 'Summer':
            return 'Remera'
        case 'Fall':
            return 'Buzo';
        case 'Winter':
            return 'Sweater';
        case 'Spring':
            return 'Camisa'
    };
};

module.exports = { getGift };
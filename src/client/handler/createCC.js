'use strict';
const { batchEventMapper } = require('ebased/handler')

const inputMode = require('ebased/handler/input/batchEventQueue');
const outputMode = require('ebased/handler/output/batchEventConfirmation');

const createCCLambda = require('../domain/createCC');

module.exports.handler = async (events, context) => {
  return batchEventMapper(
    { events, context }, 
    inputMode,
    createCCLambda, 
    outputMode);
};
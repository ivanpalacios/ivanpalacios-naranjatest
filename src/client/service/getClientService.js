const dynamo = require('ebased/service/storage/dynamo');
const config = require('ebased/util/config');

const TABLE_NAME = config.get('CLIENT_TABLE');

const getAll = async () => {
    const params = {
        TableName: TABLE_NAME
    }
    return await dynamo.scanTable(params);
};

const getOne = async (dni) => {
    const params = {
        TableName: TABLE_NAME,
        Key: { dni }
    };
    return await dynamo.getItem(params);
};

module.exports = { getOne, getAll };
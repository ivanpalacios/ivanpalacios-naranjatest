const sns = require('ebased/service/downstream/sns');
const config = require('ebased/util/config');

const NEW_CLIENT_TOPIC = config.get('NEW_CLIENT_TOPIC');

const emitClient = async (emitNewClientEvent) => {
    const { eventPayload, eventMeta } = emitNewClientEvent.get();
    const snsPublishParams = {
        TopicArn: NEW_CLIENT_TOPIC,
        Message: eventPayload
    };

    await sns.publish(snsPublishParams, eventMeta);
};

module.exports = { emitClient };
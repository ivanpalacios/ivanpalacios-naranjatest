const dynamo = require('ebased/service/storage/dynamo');
const config = require('ebased/util/config');

const TABLE_NAME = config.get('CLIENT_TABLE');

const updateService = async (item) => {
    const params = {
        TableName: TABLE_NAME,
        Key: { dni: item.dni },
        UpdateExpression: 'SET',
        ExpressionAttributeNames: {},
        ExpressionAttributeValues: {},
    };
    delete item.dni;
    Object.keys(item).forEach(key => {
        params.UpdateExpression += ` #${key} = :${key},`;
        params.ExpressionAttributeNames[`#${key}`] = key;
        params.ExpressionAttributeValues[`:${key}`] = item[key];
    });
    params.UpdateExpression = params.UpdateExpression.slice(0, -1);
    await dynamo.updateItem(params);
};

module.exports = { updateService };
const calculatePoints = (total, prev) => {
    if (typeof prev === 'undefined') prev = 0
    return Math.floor(total / 200) + prev;
};

module.exports = { calculatePoints };
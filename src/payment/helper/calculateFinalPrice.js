const calculateFinalPrice = (products, cardType) => {
    let discRate;
    let total = 0;
    if (cardType === 'CLASSIC') discRate = 0.92;
    if (cardType === 'GOLD') discRate = 0.88;
    products.forEach(product => {
        product.finalPrice = product.price * discRate
        total = total + product.finalPrice;
    });
    return {
        products,
        total
    };
};

module.exports = { calculateFinalPrice };
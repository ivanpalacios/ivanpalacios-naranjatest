const { calculatePoints } = require("../helper/calculatePoints");
const { updateClientPoints } = require("../service/updateClientPoints");
const { getClientByDNI } = require('../service/getClientService');

module.exports = async (event, _eventMeta) => {
    const { Item } = await getClientByDNI(event.dni);
    const item = {
        dni: event.dni,
        points: calculatePoints(event.total, Item.points),
        updatedAt: Date.now()
    };
    if (item.points > 0) {
        await updateClientPoints(item);
    };
    return { status: 200 };
};
const { ErrorHandled } = require("ebased/util/error");
const { CreatePaymentSchema } = require("../schema/input/createPaymentSchema");
const { getClientByDNI } = require("../service/getClientService");
const { createPaymentService } = require("../service/createPaymentService");
const { calculateFinalPrice } = require('../helper/calculateFinalPrice')
const { emitNewPayment } = require("../service/emitNewPayment");
const { EmitNewPaymentSchema } = require("../schema/event/emitNewPaymentSchema");
const { v4: uuidv4 } = require('uuid');

module.exports = async (commandPayload, commandMeta) => {
    new CreatePaymentSchema(commandPayload, commandMeta);

    const { Item } = await getClientByDNI(commandPayload.dni);
    if (!Item) throw new ErrorHandled('Client not found', { status: 400, code: "CLIENT_NOT_FOUND", layer: "DOMAIN" })
    if (!Item.active) throw new ErrorHandled('Client not active!', { status: 400, code: "CLIENT_INVALID", layer: "domain" });

    const { products, total } = calculateFinalPrice(commandPayload.products, Item.card.type);
    const payment = {
        id: uuidv4(),
        clientDni: commandPayload.dni,
        products,
        createdAt: Date.now()
    };

    await createPaymentService(payment);

    await emitNewPayment(new EmitNewPaymentSchema({ dni: commandPayload.dni, total }, commandMeta));

    return {
        status: 200,
        body: {
            message: "OK"
        }
    }
};
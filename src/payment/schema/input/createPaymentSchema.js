const { InputValidation } = require('ebased/schema/inputValidation');

class CreatePaymentSchema extends InputValidation {
    constructor(payload, meta) {
        super({
            source: meta.status,
            payload,
            specversion: "v1.0.0",
            schema: {
                strict: false,
                dni: { type: String, required: true },
                products: {
                    type: [
                        {
                            name: { type: String, required: true },
                            price: { type: Number, required: true }
                        }
                    ], required: true
                }
            }
        })
    }
}

module.exports = { CreatePaymentSchema };
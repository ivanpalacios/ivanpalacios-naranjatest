const { DownstreamEvent } = require('ebased/schema/downstreamEvent');

class EmitNewPaymentSchema extends DownstreamEvent {
    constructor(payload, meta) {
        super({
            type: 'New Payment',
            specversion: 'v1.0.0',
            payload,
            meta,
            schema: {
                strict: false,
                dni: { type: String, required: true },
                total: { type: Number, required: true }
            }
        });
    };
};

module.exports = { EmitNewPaymentSchema };
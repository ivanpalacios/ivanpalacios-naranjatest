const config = require('ebased/util/config');
const dynamo = require('ebased/service/storage/dynamo');

const TABLE_NAME = config.get('PAYMENT_TABLE');

const createPaymentService = async (item) => {
    await dynamo.putItem({ TableName: TABLE_NAME, Item: item })
};

module.exports = { createPaymentService };
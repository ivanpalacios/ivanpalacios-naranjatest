const config = require('ebased/util/config');
const dynamo = require('ebased/service/storage/dynamo');

const TABLE_NAME = config.get('CLIENT_TABLE');

const getClientByDNI = async (dni) => {
    const params = {
        TableName: TABLE_NAME,
        Key: { dni }
    };
    return await dynamo.getItem(params);
}

module.exports = { getClientByDNI };
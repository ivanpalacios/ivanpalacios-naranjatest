const sqs = require('ebased/service/downstream/sqs');
const config = require('ebased/util/config');

const NEW_PAYMENT_QUEUE = config.get('UPDATE_CLIENT_SQS');

const emitNewPayment = async (newPaymentEvent) => {
    const { eventPayload, eventMeta } = newPaymentEvent.get();
    const SQSParams = {
        QueueUrl: NEW_PAYMENT_QUEUE,
        MessageBody: eventPayload
    };
    await sqs.send(SQSParams, eventMeta)
};

module.exports = { emitNewPayment };